import os

from pynwb import get_class, load_namespaces

# Set path of the namespace.yaml file to the expected install location
ndx_fleischmann_labmetadata_specpath = os.path.join(
    os.path.dirname(__file__), "spec", "ndx-fleischmann-labmetadata.namespace.yaml"
)

# If the extension has not been installed yet but we are running directly from
# the git repo
if not os.path.exists(ndx_fleischmann_labmetadata_specpath):
    ndx_fleischmann_labmetadata_specpath = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "..",
            "spec",
            "ndx-fleischmann-labmetadata.namespace.yaml",
        )
    )

# Load the namespace
load_namespaces(ndx_fleischmann_labmetadata_specpath)

# Import for accessibility at the package level
FleischmannLabMetadata = get_class(
    "FleischmannLabMetadata", "ndx-fleischmann-labmetadata"
)
