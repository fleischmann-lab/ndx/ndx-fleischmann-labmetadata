from ndx_fleischmann_labmetadata import FleischmannLabMetadata
from pynwb.testing import TestCase
from pynwb.testing.mock.file import mock_NWBFile
from pynwb.testing.testh5io import NWBH5IOMixin

# TEMPLATE FROM:
# <https://github.com/NeurodataWithoutBorders/ndx-labmetadata-example/blob/dev/src/pynwb/tests/test_labmetadata_example.py>


def constant_metadata():
    return dict(
        lab="Fleischmann lab",
        institute="Brown University",
    )


def example_labmetadata_dict():
    return dict(
        experiment_name="passive-spontaneous",
        experiment_task="passive",
        recording_condition="head-fixed",
        imaging_type="2P",
        recording_area="PCx",
        projection_area="mPFC",
        projection_tag="green_channel",
        projection_description="all neurons recorded are mPFC-projected PCx",
    )


class TestFleischmannLabMetadata(TestCase):
    """Test basic functionality of FleischmannLabMetadata without read/write"""

    def setUp(self):
        """Set up an NWB file"""
        self.nwbfile = mock_NWBFile()

    def test_constructor(self):
        """Test that the constructor"""

        labmeta_obj = FleischmannLabMetadata(**example_labmetadata_dict())

        combined_test_meta = dict(**constant_metadata(), **example_labmetadata_dict())
        for k, v in combined_test_meta.items():
            self.assertEqual(getattr(labmeta_obj, k), v)


class TestFleischmannLabMetadataRoundtrip(NWBH5IOMixin, TestCase):
    """
    Roundtrip test for FleischmannLabMetadata to test read/write
    This test class writes the FleischmannLabMetadata to an NWBFile, then
    reads the data back from the file, and compares that the data read from file
    is consistent with the original data. Using the pynwb.testing infrastructure
    simplifies this complex test greatly by allowing to simply define how to
    create the container, add to a file, and retrieve it form a file. The
    task of writing, reading, and comparing the data is then taken care of
    automatically by the NWBH5IOMixin.
    """

    def setUpContainer(self):
        """set up example FleischmannLabMetadata object"""
        self.lab_meta_data = FleischmannLabMetadata(**example_labmetadata_dict())
        return self.lab_meta_data

    def addContainer(self, nwbfile):
        """Add the test FleischmannLabMetadata to the given NWBFile."""
        nwbfile.add_lab_meta_data(lab_meta_data=self.lab_meta_data)

    def getContainer(self, nwbfile):
        """Get the FleischmannLabMetadata object from the given NWBFile."""
        return nwbfile.get_lab_meta_data(self.lab_meta_data.name)


class TestReadmeExample(TestCase):
    """
    Run the example that is show in the README
    """

    def setUp(self) -> None:
        self.filename = "testfile.nwb"

    def tearDown(self) -> None:
        import os

        if os.path.exists(self.filename):
            os.remove(self.filename)

    def test_readme_script(self):
        from datetime import datetime
        from uuid import uuid4

        from dateutil import tz
        from ndx_fleischmann_labmetadata import FleischmannLabMetadata
        from pynwb.file import NWBFile, Subject

        from pynwb import NWBHDF5IO

        # create an example NWBFile
        nwbfile = NWBFile(
            session_description="test session description",
            identifier=str(uuid4()),
            session_start_time=datetime(1970, 1, 1, tzinfo=tz.gettz("US/Pacific")),
            subject=Subject(
                age="P50D", description="example mouse", sex="F", subject_id="test_id"
            ),
        )

        # create our custom lab metadata
        lab_meta_data = FleischmannLabMetadata(**example_labmetadata_dict())

        # Add the test FleischmannLabMetadata to the NWBFile
        nwbfile.add_lab_meta_data(lab_meta_data=lab_meta_data)

        # Write the file to disk
        filename = "testfile.nwb"
        with NWBHDF5IO(path=filename, mode="a") as io:
            io.write(nwbfile)

        # Read the file from disk
        with NWBHDF5IO(path=filename, mode="r") as io:
            in_nwbfile = io.read()
            in_lab_meta_data = in_nwbfile.get_lab_meta_data(lab_meta_data.name)
            combined_test_meta = dict(
                **constant_metadata(), **example_labmetadata_dict()
            )
            for k, v in combined_test_meta.items():
                assert getattr(lab_meta_data, k) == v
                assert getattr(in_lab_meta_data, k) == v
