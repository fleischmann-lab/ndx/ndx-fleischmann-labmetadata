# -*- coding: utf-8 -*-
import os.path

from pynwb.spec import NWBAttributeSpec, NWBGroupSpec, NWBNamespaceBuilder, export_spec


def main():
    # define version and write to file
    VERSION = "0.1.0"
    version_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "pynwb",
            "ndx_fleischmann_labmetadata",
            "VERSION",
        )
    )
    with open(version_path, "w") as f:
        f.write(VERSION)

    # define metadata
    ns_builder = NWBNamespaceBuilder(
        doc="""Fleischmann Lab Experiment Metadata""",
        name="""ndx-fleischmann-labmetadata""",
        version=VERSION,
        author=["Tuan Pham"],
        contact=["tuanhpham@brown.edu"],
    )

    ns_builder.include_type("LabMetaData", namespace="core")

    metadata = NWBGroupSpec(
        default_name="experiment_metadata",
        neurodata_type_def="FleischmannLabMetadata",
        neurodata_type_inc="LabMetaData",
        doc=("Experimental metadata for Fleischmann Lab @ Brown University."),
        quantity="*",
        attributes=[
            NWBAttributeSpec(
                name="lab",
                doc="Lab name",
                dtype="text",
                required=True,
                default_value="Fleischmann lab",
            ),
            NWBAttributeSpec(
                name="institute",
                doc="Institute or University",
                dtype="text",
                required=True,
                default_value="Brown University",
            ),
            NWBAttributeSpec(
                name="experiment_name",
                doc="General name for your set of experiment, "
                "to be easily identified or grouped later on. "
                'E.g. "closed-loop-pcx-response" or "half-arena-learning"',
                dtype="text",
                required=False,
            ),
            NWBAttributeSpec(
                name="experiment_task",
                doc="Experimental task or condition (e.g. passive, go-nogo, ...)",
                dtype="text",
                required=True,
            ),
            NWBAttributeSpec(
                name="recording_condition",
                doc="Condition of recording, whether it is head-fixed or freely moving",
                dtype="text",
                required=True,
            ),
            NWBAttributeSpec(
                name="imaging_type",
                doc="Imaging type, whether it is 1P or 2P",
                dtype="text",
                required=True,
            ),
            NWBAttributeSpec(
                name="recording_area",
                doc="Where the neural recording is from",
                dtype="text",
                required=True,
            ),
            NWBAttributeSpec(
                name="projection_area",
                doc="The target of where the `recording_area` projects to "
                "and is tagged in `projection_tag`",
                dtype="text",
                required=False,
            ),
            NWBAttributeSpec(
                name="projection_tag",
                doc="Specific channel, table, molecular tag for `projection_area`",
                dtype="text",
                required=False,
            ),
            NWBAttributeSpec(
                name="projection_description",
                doc="Further description for `projection_area` and `projection_tag`",
                dtype="text",
                required=False,
            ),
        ],
    )

    new_data_types = [metadata]

    # export the spec to yaml files in the spec folder
    output_dir = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "..", "spec")
    )
    export_spec(ns_builder, new_data_types, output_dir)
    print(
        "Spec files generated. Please make sure to rerun `pip install .` to load the changes."
    )


if __name__ == "__main__":
    # usage: python create_extension_spec.py
    main()
