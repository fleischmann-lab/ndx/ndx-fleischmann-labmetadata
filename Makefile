SHELL := /bin/bash
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

format: ## Lint format
	black src
	isort src

lint: ## Lint checks
	flake8 .
	black --check src
	isort --check src
	codespell

install: ## Create extension and install
	python src/spec/create_extension_spec.py
	pip install .

install-dev: ## Create extension and install with development dependencies
	python src/spec/create_extension_spec.py
	pip install -e .

test-import: ## Test import
	python -c "from ndx_fleischmann_labmetadata import FleischmannLabMetadata" \
		&& echo "SUCCESS" \
		|| echo "EPIC FAILED"
test: ## Run test
	pytest src/pynwb/tests -s -v

sphinx: ## Create sphinx
	cd docs && $(MAKE) html

clean: ## Clean tmp files
	find . -type f -name *.pyc -delete
	find . -type d -name __pycache__ -delete

uninstall: ## Clean uninstall
	make clean
	pip uninstall -y -r <(pip freeze --local) || echo "Uninstalled failed"
	[ ! -d "./src/pynwb/"*"/spec" ] || rm -rf src/pynwb/*/spec

fresh-install: ## Fresh install
	make uninstall
	pip install -e .[dev]
	make install-dev

version-check: ## Check consistency of version across relevant files
	./build_utils/check_version_consistency.sh

build: ## Build package
	pip install -U build
	python -m build

twine-check:
	pip install -U twine
	twine check dist/*

pypi-upload: ## Upload to Pypi
	make twine-check
	twine upload --skip-existing dist/*

pypi-check: ## Check pip install after Pypi upload
	pip install ndx-fleischmann-labmetadata
	pip show ndx-fleischmann-labmetadata
	pip check ndx-fleischmann-labmetadata
	python -c 'from ndx_fleischmann_labmetadata import AcquisitionModule'

testpypi-upload: ## Test Pypi upload using twine
	make twine-check
	twine upload --repository testpypi dist/*

testpypi-install: ## Test Pypi test installing
	pip install \
		--index-url https://test.pypi.org/simple \
		--extra-index-url https://pypi.org/simple \
		ndx-fleischmann-labmetadata

conda-recipe: ## Generate conda recipe
	pip install grayskull
	[ -f "build_utils/conda/meta.yaml" ] &&\
		echo "build_utils/conda/meta.yaml already exists" &&\
		exit 1
	grayskull pypi ndx-fleischmann-labmetadata
	[ ! -d "build_utils/conda" ] && mkdir -p build_utils/conda
	mv ndx-fleischmann-labmetadata/* build_utils/conda/
	rm ndx-fleischmann-labmetadata

conda-local-build: ## Local conda build
	conda mambabuild --no-anaconda-upload build_utils/conda

conda-build-and-upload: ## Build and upload conda package to Anaconda
	conda mambabuild \
		--token $$ANACONDA_TOKEN \
		--user $$ANACONDA_USER \
		build_utils/conda
