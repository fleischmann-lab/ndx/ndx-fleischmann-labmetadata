# `ndx-fleischmann-labmetadata`

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/fleischmann-lab/ndx/ndx-fleischmann-labmetadata?branch=main&label=pipeline&style=flat-square)](https://gitlab.com/fleischmann-lab/ndx/ndx-fleischmann-labmetadata/-/commits/main)
[![license](https://img.shields.io/gitlab/license/fleischmann-lab/ndx/ndx-fleischmann-labmetadata?color=yellow&label=license&style=flat-square)](LICENSE.txt)


[![release](https://img.shields.io/gitlab/v/release/fleischmann-lab/ndx/ndx-fleischmann-labmetadata?label=release&sort=date&style=flat-square)](https://gitlab.com/fleischmann-lab/ndx/ndx-fleischmann-labmetadata/-/releases)
[![conda package](https://img.shields.io/conda/v/fleischmannlab/ndx-fleischmann-labmetadata?color=green&style=flat-square)](https://anaconda.org/FleischmannLab/ndx-fleischmann-labmetadata)

NWB extension to store Fleischmann lab metadata.

## Requirement

The requirements and additional development requirements can be seen in [`pyproject.toml`](pyproject.toml) file.

Here are the minimum requirements:

- `python >=3.8`
- `pynwb>=1.5.0,<3`
- `hdmf>=3.4.7,<4`

## Installation

You can install `conda`:

```bash
conda install -c fleischmannlab ndx-fleischmann-labmetadata
```

Or directly from the `git` repository:

```bash
pip install git+https://gitlab.com/fleischmann-lab/ndx/ndx-fleischmann-labmetadata
```

## Usage

```python
from ndx_fleischmann_labmetadata import FleischmannLabMetadata

lab_meta_data = FleischmannLabMetadata(
    experiment_name = "passive-spontaneous",
    experiment_task = 'passive',
    recording_condition = 'head-fixed',
    imaging_type = '2P',
    recording_area = 'PCx',
    projection_area = 'mPFC',
    projection_tag = 'green_channel',
    projection_description = 'all neurons recorded are mPFC-projected PCx'
)


nwbfile.add_lab_meta_data(lab_meta_data=lab_meta_data)
```

## TODOs

- [ ] Edit `sphinx` deploy stage
- [x] Publish on `conda`


---

This extension was created using [ndx-template](https://github.com/nwb-extensions/ndx-template).
